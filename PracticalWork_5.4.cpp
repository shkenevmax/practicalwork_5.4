﻿// PracticalWork_5.4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <unordered_map>

using namespace std;

class FullCopyClass
{
public:
    int Num = 0;
    float Count = 0.0f;

    FullCopyClass() {}

    FullCopyClass(const FullCopyClass& other)
    {
        this->Num = other.Num;
        this->Count = other.Count;
    }
};

int main()
{
    // copy с применением «глубокого» копирования
    vector<FullCopyClass> OriginalVec;
    OriginalVec.resize(5);
    for (int i = 0; i < 5; i++)
    {
        FullCopyClass CopyClass;
        CopyClass.Num = i;
        CopyClass.Count = float(i) + 2.0f;
        OriginalVec[i] = CopyClass;
    }

    cout << "Copy with the use of deep copying" << endl << "Original" << endl;
    for (int i = 0; i < 5; i++)
    {
        cout << OriginalVec[i].Count << "  " << OriginalVec[i].Num << endl;
    }

    vector<FullCopyClass> CopyVec;
    CopyVec.resize(5);
    copy(OriginalVec.begin(), OriginalVec.end(), CopyVec.begin());
    cout << "FullCopy" << endl;
    for (int i = 0; i < 5; i++)
    {
        cout << CopyVec[i].Count << "  " << CopyVec[i].Num << endl;
    }
    cout << endl;

    // count_if для std::unordered_map
    unordered_map<int, string> UM_OriginalClass;
    unordered_map<int, string> UM_CopyClass;
    cout << "Unordered map original" << endl;
    for (int i = 0; i < 5; i++)
    {
        UM_OriginalClass.emplace(i, "b" + to_string(i));
        cout << UM_OriginalClass[i] << endl;
    }
    copy_if(UM_OriginalClass.cbegin(), UM_OriginalClass.cend(), inserter(UM_CopyClass, UM_CopyClass.begin()),
        [](auto const& MapInstance) {return MapInstance.second == "b4"; });
    cout << "Unordered map copy" << endl;
    for (int i = 0; i < 5; i++)
    {
        cout << UM_CopyClass[i] << endl;
    }
    cout << endl;

    // fill для заполнения массива любыми данными
    cout << "Fill for array" << endl;
    int Nums[5];
    fill(begin(Nums), begin(Nums)+5, 5u);
    for (const int num : Nums)
    {
        cout << num << endl;
    }
}
